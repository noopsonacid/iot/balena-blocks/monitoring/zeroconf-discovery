FROM balenalib/raspberrypi4-64-golang:1.17-stretch-run as base

WORKDIR /app

COPY go.* ./
COPY mdns.go ./
RUN go mod tidy
RUN CGO_ENABLED=0 go build

FROM busybox

COPY --from=base /app/mdns /usr/bin/mdns

ENV OUTPUT=/etc/prometheus/share/mdns-sd.json

CMD [ "mdns" ]
